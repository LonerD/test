//
//  ViewControllerDatail.swift
//  COPFI
//
//  Created by Дмитрий Бондаренко on 20.02.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit

class ViewControllerDatail: UIViewController {
    
    
    var allcashDVC = String()
    var currency = String()
    var allcashDVCArray = ["", "", "", "", "", ""]
    var nameOfNeeded = String()
    var descr = String()
    
    
    @IBOutlet weak var namesOfNeeded: UILabel!
    
    @IBOutlet weak var globalAllCash: UILabel!
    
    @IBAction func addMoney(sender: AnyObject) {
        alertAddMoney()
    }
    
    @IBAction func deleteMoney(sender: AnyObject) {
        alertDelMoney()
    }
    
    @IBOutlet weak var descriptionOfNeed: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        namesOfNeeded.text = nameOfNeeded
        descriptionOfNeed.text = descr
        
    }
    
    func alertAddMoney() {
        let alert = UIAlertController(title: "Сколько денег вы бы хотели бы добавить?", message: "", preferredStyle: .Alert)
        
        alert.addTextFieldWithConfigurationHandler { textField -> Void in
            textField.keyboardType = UIKeyboardType.NumberPad
            textField.contentVerticalAlignment  = UIControlContentVerticalAlignment.Center
            textField.textAlignment = NSTextAlignment.Center
        }
        alert.addAction(UIAlertAction(title: "Добавить", style: .Default) { action -> Void in
            let textf = alert.textFields![0] as UITextField
            if textf.text == "" {
                textf.text = "0"
            }
            if NSUserDefaults.standardUserDefaults().valueForKey("staticCount") != nil {
                self.allcashDVCArray = NSUserDefaults.standardUserDefaults().valueForKey("staticCount") as! [String]
                if self.allcashDVCArray.contains(self.allcashDVC) {
                    if let index = self.allcashDVCArray.indexOf(self.allcashDVC) {
                        let addingNumber = Int64(self.allcashDVC)! + Int64(textf.text!)!
                        self.allcashDVC = String(addingNumber)
                        self.allcashDVCArray[index] = String(addingNumber)
                        NSUserDefaults.standardUserDefaults().setValue(self.allcashDVCArray, forKey: "staticCount")
                            
                    }
                }
                
                self.globalAllCash.text = self.allcashDVC + " \(self.currency)"
            }
            })
        alert.addAction(UIAlertAction(title: "Выйти", style: .Cancel) { action -> Void in
            })
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func alertDelMoney() {
        let alert = UIAlertController(title: "Сколько денег вы бы хотели отнять?", message: "", preferredStyle: .Alert)
        
        alert.addTextFieldWithConfigurationHandler { textField -> Void in
            textField.keyboardType = UIKeyboardType.NumberPad
            textField.contentVerticalAlignment  = UIControlContentVerticalAlignment.Center
            textField.textAlignment = NSTextAlignment.Center
        }
        alert.addAction(UIAlertAction(title: "Выйти", style: .Cancel) { action -> Void in
            })
        alert.addAction(UIAlertAction(title: "Отнять", style: .Default) { action -> Void in
            let textf = alert.textFields![0] as UITextField
            if textf.text == "" {
                textf.text = "0"
            }
            if NSUserDefaults.standardUserDefaults().valueForKey("staticCount") != nil {
                self.allcashDVCArray = NSUserDefaults.standardUserDefaults().valueForKey("staticCount") as! [String]
                if self.allcashDVCArray.contains(self.allcashDVC) {
                    if let index = self.allcashDVCArray.indexOf(self.allcashDVC) {
                        let deleteNumber = Int64(self.allcashDVC)! - Int64(textf.text!)!
                        if Int64(textf.text!) <= Int64(self.allcashDVC) {
                            self.allcashDVC = String(deleteNumber)
                            self.allcashDVCArray[index] = String(deleteNumber)
                            NSUserDefaults.standardUserDefaults().setValue(self.allcashDVCArray, forKey: "staticCount")
                        } else {
                            self.allcashDVC = "0"
                            self.allcashDVCArray[index] = self.allcashDVC
                            NSUserDefaults.standardUserDefaults().setValue(self.allcashDVCArray, forKey: "staticCount")
                        }
                    }
                }
                self.globalAllCash.text = self.allcashDVC + " \(self.currency)"
            }
            
            })
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        let infoImage = UIImage(named: "set")
        let button:UIButton = UIButton(frame: CGRect(x: 0,y: 0,width: 25, height: 25))
        button.setBackgroundImage(infoImage, forState: .Normal)
        currencyChange()
        globalAllCash.text = allcashDVC + " \(currency)"
        button.addTarget(self, action: #selector(ViewControllerDatail.settings(_:)), forControlEvents: .TouchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
        
    }
    
    func settings (sender: UIButton) {
        presentViewController((storyboard?.instantiateViewControllerWithIdentifier("settings"))!, animated: true, completion: nil)
    }
    
    func currencyChange() {
        if NSUserDefaults.standardUserDefaults().valueForKey("currency") != nil {
            currency = NSUserDefaults.standardUserDefaults().valueForKey("currency") as! String
        } else {
            currency = "UAH"
        }
    }
}
