//
//  FirstRun.swift
//  COPFI
//
//  Created by Дмитрий Бондаренко on 25.02.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit

class FirstRun: NSObject {

    static let sharedInstance = FirstRun()
    
    var firstOpenFlag: Bool!
    
    override init() {
        firstOpenFlag = NSUserDefaults.standardUserDefaults().boolForKey("firstRun")
        if firstOpenFlag == nil {
            firstOpenFlag = false
        }
        
    }
    
    
    
}
