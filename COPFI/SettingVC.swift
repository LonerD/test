//
//  SettingVC.swift
//  COPFI
//
//  Created by Дмитрий Бондаренко on 27.02.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit

class SettingVC: UIViewController {
    
    @IBAction func DeleteData(sender: AnyObject) {
        let zeroCount = ["0", "0", "0", "0", "0", "0"]
        NSUserDefaults.standardUserDefaults().setValue(zeroCount, forKey: "staticCount")
        let alert = UIAlertController(title: "Вы сбросили все данные", message: "", preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "Продолжить", style: .Cancel) { action -> Void in
            })
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    @IBOutlet weak var setCurrency: UISegmentedControl!
    var selectedItem: Int = 1
    @IBAction func goToGlobMenu() {
        var setCurencyTitle = "UAH"
        var selectedItem: Int = 1
        if setCurrency.selectedSegmentIndex == 0 {
            setCurencyTitle = "USD"
            selectedItem = 0
        } else if setCurrency.selectedSegmentIndex == 1 {
            setCurencyTitle = "UAH"
        } else if setCurrency.selectedSegmentIndex == 2 {
            setCurencyTitle = "RUR"
            selectedItem = 2
        }
        NSUserDefaults.standardUserDefaults().setValue(setCurencyTitle, forKey: "currency")
        NSUserDefaults.standardUserDefaults().setValue(selectedItem, forKey: "selectedItem")
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    @IBAction func runTutor() {
        presentViewController((storyboard?.instantiateViewControllerWithIdentifier("tutor"))!, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if NSUserDefaults.standardUserDefaults().valueForKey("selectedItem") != nil {
            selectedItem = NSUserDefaults.standardUserDefaults().valueForKey("selectedItem") as! Int
            setCurrency.selectedSegmentIndex = selectedItem
        }
        
        
    }
    
    
}
