//
//  TableViewCell.swift
//  COPFI
//
//  Created by Дмитрий Бондаренко on 12.02.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    @IBOutlet weak var firstCP: UILabel!
    
    @IBOutlet weak var Persent: UILabel!
    
    @IBOutlet weak var NameOfPitcher: UILabel!
    
    @IBOutlet weak var AllCash: UILabel!
    
    var savedCounts = ""
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
