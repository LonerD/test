//  ViewController.swift
//  COPFI
//
//  Created by Дмитрий Бондаренко on 09.02.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    //tableView Outlet
    @IBOutlet weak var TableViewOut: UITableView!
    @IBOutlet weak var NumberInput: UITextField!
    //create customButton for returnKey of numberpad
    let button = UIButton(type: UIButtonType.Custom)
    
    var noStaticCount = ["", "", "", "", "", ""]
    var staticCount = ["", "", "", "", "", ""]
    let userDef = NSUserDefaults.standardUserDefaults()
    
    
    @IBAction func addAllCash(sender: AnyObject)  {
        if userDef.valueForKey("staticCount") != nil {
            staticCount = userDef.valueForKey("staticCount") as! [String]
        }
        for index in  0...5 {
            if staticCount[index] == "" {
                staticCount[index] = "0"
            }
            staticCount[index] = String(Int64(staticCount[index])! + Int64(noStaticCount[index])!)
        }
        userDef.setValue(staticCount, forKey: "staticCount")
        updateData()
    }
    
    @IBAction func EditAction() {
        //max count is less then 10 billons cause Int64 i dont want to write. This is not good for app. And in my opinion ten billions more then enough:)
        checkInputText()
        //check for "000"
        deleteData()
        //Update data after editing textfield
        updateData()
    }
    
    //arrays of needs
    var names = ["Самое необходимое", "Развлечения", "Сбережения", "Образование", "Резервный фонд", "Благотворительность"]
    var decription = [String]()
    var currancy = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.NumberInput.delegate = self
        customButSettings()
        putDescrData()
        removeTextNBI()  //+set image
        checkFirstRun()
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(ViewController.swipeq(_:)))
        swipe.direction = .Up
        view.addGestureRecognizer(swipe)
    }
    
    func swipeq (gesture: UIGestureRecognizer) {
        if ((gesture as? UISwipeGestureRecognizer) != nil) {
            presentViewController((storyboard?.instantiateViewControllerWithIdentifier("settings"))!, animated: true, completion: nil)
        }
    }
    
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBarHidden = true
        currencyChange()
        updateData()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        if (navigationController?.topViewController != self) {
            navigationController?.navigationBarHidden = false
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        return true
    }
    
    func currencyChange() {
        if userDef.valueForKey("currency") != nil {
            currancy = userDef.valueForKey("currency") as! String
        } else {
            currancy = "UAH"
        }
    }
    
    func checkFirstRun() {
        if FirstRun.sharedInstance.firstOpenFlag == false {
            presentViewController((storyboard?.instantiateViewControllerWithIdentifier("tutor"))!, animated: true, completion: nil)
           userDef.setBool(true, forKey: "firstRun")
           userDef.setValue(1, forKey: "selectedItem")
            staticCount = ["0", "0", "0", "0", "0", "0"]
           userDef.setValue(staticCount, forKey: "staticCount")
        }
    }
    
    func updateData() {
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            self.TableViewOut.reloadData()
        })
    }
    
    //description of needs
    func putDescrData() {
        if let description = NSBundle.mainBundle().URLForResource("DescriptionOfNeeded", withExtension: "plist") {
            if let descArray = NSArray(contentsOfURL: description) {
                decription = descArray as! [String]
            }
        }
    }
    
    func removeTextNBI() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "123", style: .Plain, target: nil, action: nil)
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "back")
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "back")
    }
    
    //custom seetings of numberpads button
    func customButSettings() {
        button.setTitle("Рассчитать", forState: UIControlState.Normal)
        button.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        button.frame = CGRectMake(0, 163, 106, 53)
        button.adjustsImageWhenHighlighted = false
        button.addTarget(self, action: #selector(ViewController.Done(_:)), forControlEvents: UIControlEvents.TouchUpInside)
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
    }
    
    func keyboardWillShow(note : NSNotification) -> Void{
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            self.button.hidden = false
            let keyBoardWindow = UIApplication.sharedApplication().windows.last
            self.button.frame = CGRectMake(0, (keyBoardWindow?.frame.size.height)!-53, 106, 53)
            keyBoardWindow?.addSubview(self.button)
            keyBoardWindow?.bringSubviewToFront(self.button)
            
            UIView.animateWithDuration(((note.userInfo! as NSDictionary).objectForKey(UIKeyboardAnimationCurveUserInfoKey)?.doubleValue)!, delay: 0, options: UIViewAnimationOptions.CurveEaseIn, animations: { () -> Void in
                self.view.frame = CGRectOffset(self.view.frame, 0, 0)
                }, completion: { (complete) -> Void in
            })
        }
    }
    
    func Done(sender : UIButton){
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            self.NumberInput.resignFirstResponder()
            self.button.removeFromSuperview()
        }
    }
    
    func checkInputText() {
        if NumberInput.text?.characters.count >= 10 {
            NumberInput.text = "9999999999"
        }
    }
    
    func deleteData() {
        if NumberInput.text == "001" {
            let alert = UIAlertController(title: "Created by", message: "Dima Bondarenko©\nЛайкай, у тебя не остается выбора:)", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Лайк, круто сделал", style: .Cancel) { action -> Void in
                })
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return names.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let DoubleNumber = (NumberInput.text! as NSString).floatValue
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! TableViewCell
        cell.NameOfPitcher.text = names[indexPath.row]
        switch indexPath.row {
        case 0 :
            let countFFPers = String(Int64(DoubleNumber * 0.55))
            cell.firstCP.text = countFFPers
            noStaticCount[0] = countFFPers
        case 5 :
            let countFivePers = String(Int64(DoubleNumber * 0.05))
            cell.firstCP.text = countFivePers
            noStaticCount[5] = countFivePers
        default :
            let countTenPers = String(Int64(DoubleNumber * 0.10))
            cell.firstCP.text = countTenPers
            for i in  1...4 {
                noStaticCount[i] = countTenPers
            }
        }
        if userDef.valueForKey("staticCount") != nil {
            let valueForGlobalCash = userDef.valueForKey("staticCount")! as! [String]
            let globalcash = valueForGlobalCash[indexPath.row]
            
            cell.AllCash.text = String(globalcash) + " \(currancy)"
        }
        return cell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.alpha = 0.2
        UIView.animateWithDuration(1, animations: {cell.alpha = 1})
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        view.endEditing(true)
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return  TableViewOut.frame.height / 6
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let dvc = segue.destinationViewController as? ViewControllerDatail {
            if let cell = sender as? UITableViewCell {
                if let indexPath = TableViewOut.indexPathForCell(cell) {
                    dvc.nameOfNeeded = names[indexPath.row]
                    dvc.descr = decription[indexPath.row]
                    if userDef.valueForKey("staticCount") != nil {
                        let value = userDef.valueForKey("staticCount")! as! [String]
                        let valueq = value[indexPath.row]
                        dvc.allcashDVC = String(valueq)
                    } else {
                        dvc.allcashDVC = "--"
                    }
                }
            }
        }
    }
    
}




