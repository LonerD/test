//
//  IVImageScroller.swift
//  G34L10
//
//  Created by Ivan Vasilevich on 2/2/16.
//  Copyright © 2016 Ivan Besarab. All rights reserved.
//

import UIKit

class IVImageScroller: UIView, UIScrollViewDelegate {
    
    var images = [UIImage]() {
        didSet {
            setup()
        }
    }
    var scrollingView: UIScrollView!
    var paggingControll: UIPageControl!
    
    
    func setup() {
        
        contentMode = .Redraw
        
        clipsToBounds = true
        
        for subView in subviews {
            subView.removeFromSuperview()
        }
        
        let scrollview = UIScrollView(frame: frame)
        scrollingView = scrollview
        scrollingView.showsHorizontalScrollIndicator = false
        scrollingView.pagingEnabled = true
        scrollview.frame.origin = CGPointZero
        scrollview.contentSize.width = frame.width * CGFloat(images.count)
        addSubview(scrollview)
        
        for i in 0..<images.count {
            let imgBox = UIImageView(frame: frame)
            imgBox.frame.origin.x = frame.width * CGFloat(i)
            imgBox.frame.origin.y = 0
            imgBox.image = images[i]
            scrollview.addSubview(imgBox)
        }
        let pageControll = UIPageControl(frame: CGRect(x: 0, y: frame.height - 40, width: frame.width, height: 37))
        addSubview(pageControll)
        pageControll.numberOfPages = images.count
        bringSubviewToFront(pageControll)
        pageControll.addTarget(self, action: "pageChanged:", forControlEvents: .ValueChanged)
        paggingControll = pageControll
        scrollingView.delegate = self
    }
    
   
    
    func pageChanged(sender: UIPageControl) {
        UIView.animateWithDuration(0.6) { () -> Void in
            self.scrollingView.contentOffset = CGPoint(x:CGFloat(sender.currentPage) * self.frame.width, y: 0)
        }
        
        
    
        
//        UIView.transitionWithView(self, duration: 1.5, options: .TransitionCurlUp, animations: { () -> Void in
//            
//            }, completion: nil)
//         scrollingView.scrollRectToVisible(CGRect(x:CGFloat(sender.currentPage) * frame.width, y: 0, width: frame.width, height: frame.height), animated: true)
//         scrollingView.scrollRectToVisible(CGRect(x: 0, y: 0, width: 100, height: 50), animated: false)
    }
    
    override func awakeFromNib() {
//        setup()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
//        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    override func drawRect(rect: CGRect) {
        // Drawing code
        setup()
    }

    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        paggingControll.currentPage = Int(scrollingView.contentOffset.x / scrollingView.frame.width)
    }

}
