//
//  tutorVC.swift
//  COPFI
//
//  Created by Дмитрий Бондаренко on 25.02.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit

class tutorVC: UIViewController {
    
    @IBOutlet weak var Ivconnect: UIImageView!
    
    @IBOutlet weak var Ivstatic: UIImageView!
    
    let backgroundImg = [UIImage(named: "20"), UIImage(named: "21"), UIImage(named: "22")]
    let tutorImg = [UIImage(named: "11"), UIImage(named: "12"), UIImage(named: "13"), UIImage(named: "14"), UIImage(named: "15")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        screen1()
    }
    
    
    
    func screen1() {
        let box = UIButton(frame: view.frame)
        box.adjustsImageWhenHighlighted = false
        box.addTarget(self, action: #selector(tutorVC.screen2(_:)), forControlEvents: .TouchUpInside)
        Ivstatic.image = tutorImg[0]
        Ivstatic.frame = view.frame
        view.addSubview(box)
    }
    
    func screen2 (sender: UIButton) {
        sender.removeFromSuperview()
        Ivconnect.image = backgroundImg[0]
        let box = UIButton(frame: view.frame)
        box.adjustsImageWhenHighlighted = false
        box.addTarget(self, action: #selector(tutorVC.screen3(_:)), forControlEvents: .TouchUpInside)
        view.addSubview(box)
        Ivstatic.alpha = 0
        Ivstatic.image = tutorImg[1]
        UIView.animateWithDuration(1) { () -> Void in
            self.Ivstatic.alpha = 1
        }
        view.addSubview(box)
    }
    
    func screen3 (sender: UIButton) {
        sender.removeFromSuperview()
        Ivconnect.image = backgroundImg[1]
        let box = UIButton(frame: view.frame)
        box.adjustsImageWhenHighlighted = false
        box.addTarget(self, action: #selector(tutorVC.screen4(_:)), forControlEvents: .TouchUpInside)
        view.addSubview(box)
        Ivstatic.alpha = 0
        Ivstatic.image = tutorImg[2]
        UIView.animateWithDuration(1) { () -> Void in
            self.Ivstatic.alpha = 1
        }
        view.addSubview(box)
    }
    
    func screen4 (sender: UIButton) {
        sender.removeFromSuperview()
        Ivconnect.image = backgroundImg[2]
        let box = UIButton(frame: view.frame)
        box.adjustsImageWhenHighlighted = false
        box.addTarget(self, action: #selector(tutorVC.screen5(_:)), forControlEvents: .TouchUpInside)
        view.addSubview(box)
        Ivstatic.alpha = 0
        Ivstatic.image = tutorImg[3]
        UIView.animateWithDuration(1) { () -> Void in
            self.Ivstatic.alpha = 1
        }
        view.addSubview(box)
    }
    
    func screen5 (sender: UIButton) {
        sender.removeFromSuperview()
        Ivconnect.image = backgroundImg[1]
        let box = UIButton(frame: view.frame)
        box.adjustsImageWhenHighlighted = false
        box.addTarget(self, action: #selector(tutorVC.dismisVC(_:)), forControlEvents: .TouchUpInside)
        view.addSubview(box)
        Ivstatic.alpha = 0
        Ivstatic.image = tutorImg[4]
        UIView.animateWithDuration(1) { () -> Void in
            self.Ivstatic.alpha = 1
        }
        view.addSubview(box)
    }
    
    
    func dismisVC (sender: UIButton) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    
    
    
}
